import os
import io
import sys
import time
import base64
import tkinter
import numpy as np
import tensorflow as tf
from PIL import ImageTk, Image
from keras.layers import *
from keras.models import Model, load_model
import matplotlib.pyplot as plt
from visualize import plot_hand
from Autoencoder import AutoEncoder
from AutoEncoderQuat import AutoEncoderQuat

model_save_path = 'explorer'
coding_dims = [40,30,20,10,5,2]

def load_data(path):
    train = np.genfromtxt(os.path.join(path,'training.csv'),
        delimiter=',')[1:]
    train = train / np.max(train)
    #test = np.genfromtxt(os.path.join(path,'testing.csv'),
    #    delimiter=',')[1:]

    return {
        'train': train,
        #'test': test
    }

def make_and_train(data, coding_dims, num_epochs, learning_rate, load=True):
    net = AutoEncoder(input_dim = data['train'].shape[1],
                          coding_dims = coding_dims,
                          learning_rate = learning_rate)

    filepath = model_save_path + '_'.join(str(dim) for dim in coding_dims) + '.h5'

    if load and os.path.isfile(filepath):
        net.load_from_file(filepath)
        print("Loaded model file")
    else:
        net.train_epochs(x = data['train'], y = data['train'],
                        epochs = num_epochs,
                        repetitions = 1,
                        validation_data = (data['train'], data['train']))

        net.save_to_file(filepath)

    return net

def get_space_bounds(net, data):
    encodings = net.encode(data['train'], no_xyz=True)
    max_x = max(encodings, key=lambda v:v[0])[0]
    min_x = min(encodings, key=lambda v:v[0])[0]
    max_y = max(encodings, key=lambda v:v[1])[1]
    min_y = min(encodings, key=lambda v:v[1])[1]

    return {
        'max_x': max_x,
        'max_y': max_y,
        'min_x': min_x,
        'min_y': min_y
    }

def scale(x, y, space_bounds):
    root = tkinter.Tk()
    x /= root.winfo_screenwidth()
    y /= root.winfo_screenheight()
    x *= (space_bounds['max_x'] - space_bounds['min_x'])
    y *= (space_bounds['max_y'] - space_bounds['min_y'])
    x += space_bounds['min_x']
    y += space_bounds['min_y']
    root.destroy()

    return x, y

def create_window():
    window = tkinter.Tk()
    close_button = tkinter.Button(window,
                        height = 1,
                        padx = 5,
                        pady = 5,
                        text = 'X',
                        command = window.destroy).pack()
    hand_canvas = tkinter.Canvas(window,
                        height = 600,
                        width = 600)
    hand_canvas.pack()
    space_canvas = tkinter.Canvas(window,
                        height = 600,
                        width=600)
    space_canvas.pack()
    return window, hand_canvas, space_canvas

def render_image(canvas, net, coord):
    hand = net.decode(coord)[0]
    fig = plot_hand(hand, figsize=(6,6))
    pltcanvas = plt.get_current_fig_manager().canvas
    pltcanvas.draw()
    pil_image = Image.frombytes('RGB', pltcanvas.get_width_height(),
        pltcanvas.tostring_rgb())

    tk_img = ImageTk.PhotoImage(image=pil_image)
    canvas.image = tk_img
    canvas.create_image(0, 0, image=tk_img, anchor=tkinter.NW)
    plt.close(fig)

def render_space_canvas(canvas, net, data):
    print('Plotting space...')
    encodings = net.encode(data['train'], no_xyz=True)
    fig = plt.figure(figsize=(6,6))
    ax = fig.add_subplot(111)
    ax.scatter(encodings[:,0], encodings[:,1], s=0.2)
    pltcanvas = plt.get_current_fig_manager().canvas
    pltcanvas.draw()
    pil_image = Image.frombytes('RGB', pltcanvas.get_width_height(),
        pltcanvas.tostring_rgb())

    tk_img = ImageTk.PhotoImage(image=pil_image)
    canvas.image = tk_img
    canvas.create_image(0, 0, image=tk_img, anchor=tkinter.NW)
    plt.close(fig)

imgbuf = [None]
def loop_render(window, canvas, net, space_bounds, framedelay):
    mousex = window.winfo_pointerx()
    mousey = window.winfo_pointery()
    x, y = scale(mousex, mousey, space_bounds)
    coord = np.expand_dims([x,y], axis=0)
    render_image(canvas, net, coord)

    window.after(framedelay,
        lambda: loop_render(window,canvas,net,space_bounds,framedelay))

def begin(framedelay=16):
    window, h_canvas, s_canvas = create_window()
    data = load_data('')
    net = make_and_train(data, coding_dims, 50, 1e-6)
    render_space_canvas(s_canvas, net, data)
    space_bounds = get_space_bounds(net, data)
    loop_render(window, h_canvas, net, space_bounds, framedelay)
    window.mainloop()


begin(30)

