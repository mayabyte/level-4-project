import os, sys, time
import Leap

data_folder = 'data'
frame_delay = 0.02
finger_bones = (Leap.Bone.TYPE_METACARPAL, Leap.Bone.TYPE_PROXIMAL,
                Leap.Bone.TYPE_INTERMEDIATE, Leap.Bone.TYPE_DISTAL)

def vec2str(vec, sep=','):
    return sep.join(str(coord) for coord in vec.to_float_array())

def wait_next_frame():
    time.sleep(frame_delay)

def main():
    if not os.path.isdir(data_folder):
        os.mkdir(data_folder)
    gesture = raw_input('Which gesture will be recorded? ')
    datafile = os.path.join(data_folder, gesture + '.csv')

    controller = Leap.Controller()
    while not controller.is_connected:
        pass
    print('Controller connected.\nMove hand over Leap Motion to collect data.')

    collected_hands = 0

    with open(datafile, 'a+') as file:
        while controller.is_connected:
            frame = controller.frame()
            if len(frame.hands) != 1 or not frame.is_valid:
                wait_next_frame()
                continue

            hand = frame.hands[0]
            if hand.confidence < 0.75:
                wait_next_frame()
                continue
            # hand will always be valid if it has a confidence above 0

            if len(hand.fingers) != 5:
                wait_next_frame()
                continue

            wrist_pos = hand.wrist_position
            palm_pos = hand.palm_position
            # palm_pos = hand.stabilized_palm_position
            # maybe normalize all the positions by one of these?

            fingerbone_positions = [[finger.bone(bone).next_joint
                for bone in finger_bones] for finger in hand.fingers]
            fb_pos_flat = reduce(lambda x,y: x+y, fingerbone_positions)

            all_bones = [wrist_pos, palm_pos] + fb_pos_flat
            all_bones_str = ','.join(vec2str(bone) for bone in all_bones) + '\n'

            # final bone order will be
            # wrist, palm, thumb1234, index1234, middle1234, ring1234, pinky1234

            # given bone order is
            # palm, thumb123, index123, middle123, ring123, pinky123, thumb4, index4, middle4, ring4, pinky4

            file.write(all_bones_str)
            collected_hands += 1
            print(str(collected_hands) +
                ' hands collected this session')
            wait_next_frame()



if __name__ == "__main__":
    main()