import os
import sys
import time
import numpy as np
import Leap
from Autoencoder import AutoEncoder
from sklearn.cluster import KMeans

finger_bones = (Leap.Bone.TYPE_METACARPAL, Leap.Bone.TYPE_PROXIMAL,
                Leap.Bone.TYPE_INTERMEDIATE, Leap.Bone.TYPE_DISTAL)

def make_model(coding_dims, data, learning_rate=1e-7, num_epochs=50, batch_size=32):
    # make sure data is normalized
    data = data / np.max(data)

    model = AutoEncoder(input_dim = data.shape[1],
        coding_dims = coding_dims,
        learning_rate = learning_rate,
        batch_size = batch_size)

    model.train_epochs(x = data, y = data,
        epochs = num_epochs,
        repetitions = 1,
        validation_data = (data,data))

    return model

def save_model(model, filename):
    model.save_to_file(filename)

def load_model_into(model, filename):
    if os.path.isfile(filename):
        model.load_from_file(filename)
        return model
    else:
        print('No such file {}'.format(filename))
        sys.exit(1)


def find_num_clusters(model, data, check_to=10):
    encoded_hands = model.encode(data)
    num_clusters_loss = []
    for i in range(1,check_to):
        km = KMeans(n_clusters=i, n_jobs=-1)
        kmvals = km.fit_transform(encoded_hands)
        kmloss = np.sum(kmvals.min(axis=1)**2)
        num_clusters_loss.append(kmloss)

    n_clusters = len(list(filter(lambda x: x>100,
                                 np.diff(np.gradient(num_clusters_loss)))))-1

    return n_clusters

def get_km(n_clusters, model, data):
    km = KMeans(n_clusters=n_clusters, n_jobs=-1)
    enc_hands = model.encode(data)
    km.fit(enc_hands)
    return km

def predict_sample(km, sample):
    if len(sample.shape) == 1:
        return km.predict(np.expand_dims(sample, axis=0))[0]
    else:
        return km.predict(sample)[0]

def wait_next_frame():
    time.sleep(0.02)

def collect_hand_skeleton(controller):
    while True:
        frame = controller.frame()
        if len(frame.hands) != 1 or not frame.is_valid:
            wait_next_frame()
            continue

        hand = frame.hands[0]
        if hand.confidence < 0.75:
            wait_next_frame()
            continue
        # hand will always be valid if it has a confidence above 0

        if len(hand.fingers) != 5:
            wait_next_frame()
            continue

        wrist_pos = hand.wrist_position
        palm_pos = hand.palm_position
        # palm_pos = hand.stabilized_palm_position
        # maybe normalize all the positions by one of these?

        fingerbone_positions = [[finger.bone(bone).next_joint
            for bone in finger_bones] for finger in hand.fingers]
        fb_pos_flat = reduce(lambda x,y: x+y, fingerbone_positions)

        all_bones = [wrist_pos, palm_pos] + fb_pos_flat
        all_bones_xyz = [coord for bone in all_bones for coord in bone.to_float_array()]

        return np.asarray(all_bones_xyz)



def main():
    data = np.genfromtxt('training.csv', delimiter=',')
    model = make_model(coding_dims = [40,30,20,10],
                       data = data,
                       num_epochs=0)
    model = load_model_into(model, 'run1.h5')

    n_clusters = find_num_clusters(model, data)
    print('num clusters: {}'.format(n_clusters))

    km = get_km(n_clusters, model, data)
    controller = Leap.Controller()

    while not controller.is_connected:
        pass
    print('leap driver connected.')

    while True:
        hand = collect_hand_skeleton(controller)
        enc = model.encode(np.expand_dims(hand, axis=0))
        pred = predict_sample(km, enc)
        print(pred)
        wait_next_frame()


if __name__ == '__main__':
    main()
