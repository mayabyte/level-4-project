Maya's Level 4 Project. 

Most code is contained in Jupyter Notebooks. Trained model files are saved in the models/ folder and can be loaded to avoid training models yourself.

IMPORTANT: libLeap.so, LeapPython.so, Leap.py, and everything in leap_sdk/ are NOT MY WORK. They are required library files for the Leap Motion device used to interact with Python code. They are manually compiled to run on MacOS and thus the portions of code that rely on these libraries may not work on other operating systems or architectures. Additionally, some code for the TSNE autoencoder is also not my work. The cells that aren't mine are clearly marked with comments saying where they came from.