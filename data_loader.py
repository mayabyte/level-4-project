import numpy as np
import glob
import os

def load_data():
    data_path = 'data'
    files = glob.glob(os.path.join(data_path, '*.csv'))
    print(list(enumerate(files)))
    datasets = [np.genfromtxt(file, delimiter=',') for file in files]
    labels = np.concatenate([[i]*len(ds) for i,ds in enumerate(datasets)], axis=0)
    data = np.concatenate(datasets, axis=0)

    # center all wrist positions to 0,0
    data = data - np.expand_dims(data[:,0],axis=-1)

    # normalize
    factor = np.max(data)
    data /= factor

    return data, labels, factor