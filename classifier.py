import numpy as np
from sklearn import svm

def get_classifier(encoder, data, labels, verbose=False):
    pct_idx = int(0.8*len(data))
    shuffled_idx = np.arange(len(data))
    np.random.shuffle(shuffled_idx)
    data_shuffled = np.copy(data)[shuffled_idx]
    labels_shuffled = np.copy(labels)[shuffled_idx]

    test_labels = labels_shuffled[pct_idx:]
    train_labels = labels_shuffled[:pct_idx]

    if verbose:
        # unencoded accuracy
        train = data_shuffled[:pct_idx]
        test = data_shuffled[pct_idx:]
        

        classifier = svm.SVC(gamma='scale')
        classifier.fit(train, train_labels)

        preds = classifier.predict(test)
        accuracy = np.sum(preds == test_labels)/len(test)
        print('unencoded accuracy:\t',accuracy)

    # encoded accuracy
    train = encoder.predict(data_shuffled[:pct_idx])
    test = encoder.predict(data_shuffled[pct_idx:])

    if len(train) == 3:
        train = train[0]
        test = test[0]

    classifier = svm.SVC(gamma='scale')
    classifier.fit(train, train_labels)

    preds = classifier.predict(test)
    accuracy = np.sum(preds == test_labels)/len(test)
    if verbose:
        print('encoded accuracy:\t',accuracy)

    return classifier