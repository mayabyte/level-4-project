import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

data_path = 'data'

# Get metadata
node_names = ['wrist', 'palm', 'thumb_root', 'thumb_mid', 'thumb_int', 'thumb_tip', 'index_root', 'index_mid', 'index_int', 'index_tip', 'middle_root', 'middle_mid', 'middle_int', 'middle_tip', 'ring_root', 'ring_mid', 'ring_int', 'ring_tip', 'pinky_root', 'pinky_mid', 'pinky_int', 'pinky_tip']
node_name_indices = dict(zip(node_names,range(len(node_names))))

# Define which bones are connected
# _tip is the last joint, _end is the tip of the finger
bone_connections = [['wrist','thumb_root'], ['index_root', 'middle_root'],
                    ['middle_root', 'ring_root'], ['ring_root', 'pinky_root'],
                    ['thumb_root', 'index_root'], ['wrist', 'pinky_root'],
                    ['wrist', 'thumb_root'], ['palm', 'index_root'],
                    ['palm', 'middle_root'], ['palm', 'ring_root'],
                    ['palm', 'pinky_root'], ['thumb_root', 'thumb_mid'],
                    ['thumb_mid', 'thumb_int'], ['index_root', 'index_mid'],
                    ['index_mid', 'index_int'], ['middle_root', 'middle_mid'],
                    ['middle_mid', 'middle_int'], ['ring_root', 'ring_mid'],
                    ['ring_mid', 'ring_int'], ['pinky_root', 'pinky_mid'],
                    ['pinky_mid', 'pinky_int'], ['thumb_int', 'thumb_tip'],
                    ['index_int', 'index_tip'], ['middle_int', 'middle_tip'],
                    ['ring_int', 'ring_tip'], ['pinky_int', 'pinky_tip']]

def unflatten(arr,n=3):
    return list(zip(*(iter(arr),)*n))

def plot_hand(skeleton, figsize=(6,6), is_flattened=True, color='b'):
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, projection='3d')

    if is_flattened:
        skeleton = unflatten(skeleton)

    for point in skeleton:
        ax.scatter(*point, c=color)

    for connection in bone_connections:
        ax.plot([skeleton[node_name_indices[connection[0]]][0],
                 skeleton[node_name_indices[connection[1]]][0]],
                [skeleton[node_name_indices[connection[0]]][1],
                 skeleton[node_name_indices[connection[1]]][1]],
                [skeleton[node_name_indices[connection[0]]][2],
                 skeleton[node_name_indices[connection[1]]][2]], c=color)

    return fig

def plot_reconstruction(skel1, skel2, figsize=(16,16), is_flattened=True):
    fig = plt.figure(figsize=figsize)
    ax = fig.add_subplot(111, projection='3d')

    if is_flattened:
        skel1 = unflatten(skel1)
        skel2 = unflatten(skel2)

    for point1, point2 in zip(skel1, skel2):
        ax.scatter(*point1, c='b')
        ax.scatter(*point2, c='r')

    for connection in bone_connections:
        ax.plot([skel1[node_name_indices[connection[0]]][0],
                 skel1[node_name_indices[connection[1]]][0]],
                [skel1[node_name_indices[connection[0]]][1],
                 skel1[node_name_indices[connection[1]]][1]],
                [skel1[node_name_indices[connection[0]]][2],
                 skel1[node_name_indices[connection[1]]][2]], c='b')
        ax.plot([skel2[node_name_indices[connection[0]]][0],
                 skel2[node_name_indices[connection[1]]][0]],
                [skel2[node_name_indices[connection[0]]][1],
                 skel2[node_name_indices[connection[1]]][1]],
                [skel2[node_name_indices[connection[0]]][2],
                 skel2[node_name_indices[connection[1]]][2]], c='r')


