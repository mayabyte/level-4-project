import pynput
import time
import Leap
from functools import reduce
import keras
import numpy as np
from classifier import get_classifier
from data_loader import load_data

finger_bones = (Leap.Bone.TYPE_METACARPAL, Leap.Bone.TYPE_PROXIMAL,
                Leap.Bone.TYPE_INTERMEDIATE, Leap.Bone.TYPE_DISTAL)
mouse_speed = 3

def main():
    mouse = pynput.mouse.Controller()
    leap = Leap.Controller()
    leap.set_policy(Leap.Controller.POLICY_BACKGROUND_FRAMES)
    encoder = keras.models.load_model('models/basic_enc.h5')

    print('Loading data... ', end='')
    data, labels, factor = load_data()
    print('done.')

    print('Setting up classifier... ', end='')
    classifier = get_classifier(encoder, data, labels, verbose=False)
    print('done.')

    while not leap.is_connected:
        pass
    print('Leap Motion connected.')

    print('\nPINKY OUT \t= DOWN\nTHUMB OUT \t= RIGHT\nFLAT HAND \t= LEFT\nPOINTER OUT \t= UP\nTHUMB & POINTER = CLICK')

    gesture_history = []
    while True:
        # collect leap hand data 
        frame = leap.frame()
        if len(frame.hands) != 1 or not frame.is_valid:
            wait_next_frame()
            continue
        skeleton = get_hand_skeleton(frame, factor)
        if skeleton is None:
            wait_next_frame()
            continue

        # encode hand data and classify it 
        enc_skeleton = encoder.predict(np.expand_dims(skeleton, axis=0))
        gesture = classifier.predict(enc_skeleton)[0]
        gesture_history.append(gesture)
        print('_class:', gesture, end='\r')

        # switch on gesture to perform action
        stable_gesture = last_n_frames(gesture_history, n=5)
        if stable_gesture is None:
            wait_next_frame()
            continue 
        # MOVE MOUSE DOWN
        elif stable_gesture == 0:
            mouse.move(0, mouse_speed)
        # MOVE MOUSE RIGHT
        elif stable_gesture == 1:
            mouse.move(mouse_speed, 0)
        # MOVE MOUSE LEFT
        elif stable_gesture == 2:
            mouse.move(-mouse_speed, 0)
        # MOVE MOUSE UP
        elif stable_gesture == 3:
            mouse.move(0, -mouse_speed)
        # LEFT CLICK MOUSE
        elif stable_gesture == 4 and \
            last_n_frames(gesture_history, n=8) != stable_gesture:
            mouse.click(pynput.mouse.Button.left)

        wait_next_frame()

def get_hand_skeleton(frame, factor):
    hand = frame.hands[0]

    if hand.confidence < 0.5:
        return None 

    if len(hand.fingers) != 5:
        return None

    wrist_pos = hand.wrist_position
    palm_pos = hand.palm_position

    fingerbone_positions = [[finger.bone(bone).next_joint
        for bone in finger_bones] for finger in hand.fingers]
    fb_pos_flat = reduce(lambda x,y: x+y, fingerbone_positions)

    all_bones = [wrist_pos, palm_pos] + fb_pos_flat
    all_bones = [bone - all_bones[0] for bone in all_bones]
    all_bones_xyz = []
    for bone in all_bones:
        all_bones_xyz.extend([coord for coord in bone.to_float_array()])

    return np.array(all_bones_xyz) / factor

def last_n_frames(gesture_history, n=5):
    if all(v == gesture_history[-1] for v in gesture_history[-n:]):
        return gesture_history[-1]
    else:
        return None

# 60FPS
def wait_next_frame():
    time.sleep(0.016)

if __name__ == '__main__':
    main()